﻿using lab11_mvvm.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace lab11_mvvm
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            btnOperaciones.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new Operations());
            };

        }
    }
}
